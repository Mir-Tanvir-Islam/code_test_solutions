class Person(object):
	def __init__(self, first_name, last_name, father):
		self.first_name = first_name
		self.last_name = last_name
		self.father = father

	def keys(self):
		person_dict = {
			'first_name' : self.first_name,
			'last_name' : self.last_name,
			'father' : self.father
		}
		return person_dict

	def __getitem__(self, token):
		person_dict = {
			'first_name' : self.first_name,
			'last_name' : self.last_name,
			'father' : self.father
		}
		return person_dict[token]


def depth_find(object, i=1):
	try:
		for key in object.keys():
			depth_dict[key] = i
			depth_find(object[key], i+1)
	except:
		pass


person_a = Person("User", "1", None)
person_b = Person("User", "2", person_a)

a = { 
	"key1": 1,
	"key2": {
		"key3": 1,
		"key4": {
			"key5": 4,
			"user": person_b
			}
	} 
} 

depth_dict = {}
depth_find(a)
print('depth_dict: ', depth_dict)
