def depth_find(object, i=1):
	if not isinstance(object, int):
		for key in object.keys():
			depth_dict[key] = i
			depth_find(object[key], i+1)


a = {
	"key1": 1,
	"key2": {
		"key3": 1,
		"key4": {
			"key5": 4
			}
	}
}

depth_dict = {}
depth_find(a)
print('depth_dict: ', depth_dict)