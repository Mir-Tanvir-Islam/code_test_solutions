class Node:
	def __init__(self, data):
		self.data = data
		self.parent = None

	def __repr__(self):
		return str(self.data)


def lca(n1, n2):
	root = n1
	n1_traceback = [root]
	while root.parent:
		root = root.parent
		n1_traceback.append(root)

	root = n2
	while root.parent:
		root = root.parent
		if root in n1_traceback:
			break

	return root


n1 = Node(1)
n2 = Node(2)
n3 = Node(3)
n4 = Node(4)
n5 = Node(5)
n6 = Node(6)
n7 = Node(7)
n8 = Node(8)
n9 = Node(9)
n8.parent = n4
n9.parent = n4
n4.parent = n2
n5.parent = n2
n6.parent = n3
n7.parent = n3
n3.parent = n1
n2.parent = n1

print("LCA :", lca(n6, n7))
print("LCA :", lca(n3, n7))

